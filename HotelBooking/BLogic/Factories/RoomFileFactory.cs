﻿using BLogic.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BLogic.Factories
{
    public class RoomFileFactory
    {
        public string Path { get; set; }
        FileStream fs;
        XmlSerializer xs;

        public RoomFileFactory()
        {
            Path = "CollectionRoom.XML";
        }

        public void Save(RoomCollection collection)
        {
            if (File.Exists(Path))
            {
                File.Delete(Path);
            }

            fs = new FileStream(Path, FileMode.Create);
            xs = new XmlSerializer(typeof(RoomCollection), new Type[] { typeof(Room) });

            xs.Serialize(fs, collection);
            fs.Close();

            collection.GetEnumerator().Reset();
        }

        public RoomCollection Open()
        {
            if (File.Exists(Path))
            {
                fs = new FileStream(Path, FileMode.Open);
                xs = new XmlSerializer(typeof(RoomCollection));
                RoomCollection collection = new RoomCollection();

                collection = (RoomCollection)xs.Deserialize(fs);
                fs.Close();
                collection.GetEnumerator().Reset();
                return collection;
            }

            else throw new FileNotFoundException();
        }
    }
}
