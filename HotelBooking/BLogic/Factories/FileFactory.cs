﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLogic.Classes;
using BLogic.Interfaces;

namespace BLogic.Factories
{
    public class FileFactory
    {
        private RoomFileFactory _roomFile;
        private ClientFileFactory _clientFile;
        private delegate void SaveDelegate(IContract contract);
        private delegate IContract OpenDelegate();
        private Dictionary<Type, SaveDelegate> _dictionarySave;
        private Dictionary<Type, OpenDelegate> _dictionaryOpen;

        public ClientFileFactory ClientFile
        {
            get
            {
                return _clientFile;
            }

            set
            {
                _clientFile = value;
            }
        }

        public RoomFileFactory RoomFile
        {
            get
            {
                return _roomFile;
            }

            set
            {
                _roomFile = value;
            }
        }

        public FileFactory()
        {
            ClientFile = new ClientFileFactory();
            RoomFile = new RoomFileFactory();

            _dictionarySave = new Dictionary<Type, SaveDelegate>
            {
                {typeof(ClientCollection), this.SaveClient },
                {typeof(RoomCollection), this.SaveRoom }
            };

            _dictionaryOpen = new Dictionary<Type, OpenDelegate>
            {
                {typeof(ClientCollection), this.OpenClient },
                {typeof(RoomCollection), this.OpenRoom }
            };
        }

        public void Save(IContract collection)
        {
            _dictionarySave[collection.GetType()](collection);
        }

        public IContract Open(IContract collection)
        {
            return _dictionaryOpen[collection.GetType()]();
        }

        private void SaveClient(IContract collection)
        {
            ClientFile.Save((ClientCollection)collection);
        }

        private IContract OpenClient()
        {
            return ClientFile.Open();
        }

        private void SaveRoom(IContract collection)
        {
            RoomFile.Save((RoomCollection)collection);
        }

        private IContract OpenRoom()
        {
            return RoomFile.Open();
        }
    }
}
