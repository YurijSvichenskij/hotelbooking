﻿using BLogic.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BLogic.Factories
{
    public class ClientFileFactory
    {
        public string Path { get; set; }
        FileStream fs;
        XmlSerializer xs;

        public ClientFileFactory()
        {
            Path = "CollectionClient.XML";
        }

        public void Save(ClientCollection collection)
        {
            if (File.Exists(Path))
            {
                File.Delete(Path);
            }

            fs = new FileStream(Path, FileMode.Create);
            xs = new XmlSerializer(typeof(ClientCollection), new Type[] { typeof(Client) });

            xs.Serialize(fs, collection);
            fs.Close();
            collection.GetEnumerator().Reset();
        }

        public ClientCollection Open()
        {
            if (File.Exists(Path))
            {
                fs = new FileStream(Path, FileMode.Open);
                xs = new XmlSerializer(typeof(ClientCollection));
                ClientCollection collection = new ClientCollection();

                collection = (ClientCollection)xs.Deserialize(fs);
                fs.Close();
                collection.GetEnumerator().Reset();
                return collection;
            }

            else throw new FileNotFoundException();
        }
    }
}
