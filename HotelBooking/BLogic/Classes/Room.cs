﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLogic.Classes
{
    public class Room:IComparable<Room>, IEquatable<Room>
    {
        private int _id;
        private int _cost;
        private string _type;
        private bool _busy;

        public Room()
        {
            _busy = false;
        }

        public Room(int id, int cost, string type)
        {
            _id = id;
            _cost = cost;
            _type = type;
            _busy = false;
        }

        public int Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
            }
        }

        public int Cost
        {
            get
            {
                return _cost;
            }

            set
            {
                _cost = value;
            }
        }

        public string Type
        {
            get
            {
                return _type;
            }

            set
            {
                _type = value;
            }
        }

        public bool Busy
        {
            get
            {
                return _busy;
            }

            set
            {
                _busy = value;
            }
        }

        public int CompareTo(Room other)
        {
            if (this.Id > other.Id) return 1;
            else if (this.Id < other.Id) return -1;
            else return 0;
        }

        public bool Equals(Room other)
        {
            return this.Id == other.Id;
        }
    }
}
