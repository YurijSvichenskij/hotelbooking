﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using BLogic.Interfaces;

namespace BLogic.Classes
{

    public class ClientCollection : IList<Client>, IContract

    {
        private Client[] _clientArray;

        public ClientCollection()
        {
            _clientArray = new Client[0];
        }
        public Client this[int index]
        {
            get
            {
                return _clientArray[index];
            }

            set
            {
                _clientArray[index] = value;
            }
        }
        public int Count
        {
            get
            {
                return _clientArray.Length;
            }
        }
        public bool IsReadOnly
        {
            get
            {
                return _clientArray.IsReadOnly;
            }
        }
        public void Add(Client item)
        {
            foreach (var some in _clientArray)
            {
                if (item.Equals(some)) throw new Exception("This client already exists.");
            }
            var length = _clientArray.Length;
            length++;
            Client[] temp = new Client[length];
            Array.Copy(_clientArray, temp, _clientArray.Length);
            temp[length - 1] = item;
            _clientArray = temp;
        }

        public void Clear()
        {
            _clientArray = new Client[0];
        }

        public bool Contains(Client item)
        {
            foreach (var some in _clientArray)
                if (some.Equals(item))
                    return true;
            return false;
        }
        public void CopyTo(Client[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }
        public IEnumerator<Client> GetEnumerator()
        {
            return new ClientEnum(_clientArray);
        }

        public int IndexOf(Client item)
        {
            for (int i = 0; i < _clientArray.Length; i++)
            {
                if (_clientArray[i].Equals(item))
                    return i;
            }
            return -1;
        }

        public void Insert(int index, Client item)
        {
            _clientArray[index] = item;
        }

        public bool Remove(Client item, ref RoomCollection collection)
        {
            Room troom = new Room();
            var retnum = new bool();
            Client[] temp = new Client[_clientArray.Length - 1];

            for (int i = 0, j = 0; i < _clientArray.Length; i++)
            {
                if (!_clientArray[i].Equals(item))
                {
                    temp[j] = _clientArray[i];
                    j++;
                }
                else
                {
                    retnum = true;
                    troom.Id = _clientArray[i].Id;
                }
            }

            _clientArray = temp;

            for (int i = 0; i < collection.Count; i++)
            {
                if (collection[i].Equals(troom))
                {
                    collection[i].Busy = false;
                    break;
                }
            }

            return retnum;
        }

        public void RemoveAt(int index)
        {
            Client[] temp = new Client[_clientArray.Length - 1];
            for (int i = 0, j = 0; i < _clientArray.Length; i++)
            {
                if (i != index)
                {
                    temp[j] = _clientArray[i];
                    j++;
                }
            }
            _clientArray = temp;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Sort()
        {
            for (int i = 0; i < _clientArray.Length; i++)
            {
                for (int j = 0; j < _clientArray.Length; j++)
                {
                    if (_clientArray[i].CompareTo(_clientArray[j]) == 1)
                    {
                        Client temp = _clientArray[j];
                        _clientArray[j] = _clientArray[i];
                        _clientArray[i] = temp;
                    }
                }
            }
        }

        public bool Remove(Client item)
        {
            throw new NotImplementedException();
        }
    }
}
