﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLogic.Classes
{
    public class RoomEnum : IEnumerator<Room>
    {
        private Room[] _roomArray;
        private static int _position = -1;

        public RoomEnum(Room[] roomArray)
        {
            _roomArray = roomArray;
        }

        public static int Position
        {
            get
            {
                return _position;
            }

            set
            {
                _position = value;
            }
        }

        public Room Current
        {
            get
            {
                return _roomArray[_position];
            }
        }

        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public bool MoveNext()
        {
            _position += 1;
            return (_position < _roomArray.Length);
        }

        public void Reset()
        {
            _position = -1;
        }
    }
}
