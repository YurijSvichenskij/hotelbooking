﻿using BLogic.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLogic.Classes
{
    public class RoomCollection : IList<Room>, IContract
    {
        private Room[] _roomArray;

        public RoomCollection()
        {
            _roomArray = new Room[0];
        }

        public Room this[int index]
        {
            get
            {
                return _roomArray[index];
            }

            set
            {
                _roomArray[index] = value;
            }
        }

        public int Count
        {
            get
            {
                return _roomArray.Length;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return _roomArray.IsReadOnly;
            }
        }

        public void Add(Room item)
        {
            foreach (var some in _roomArray)
            {
                if (item.Equals(some)) throw new Exception("This room is already exists.");
            }

            var length = _roomArray.Length;
            length++;
            Room[] temp = new Room[length];
            Array.Copy(_roomArray, temp, _roomArray.Length);
            temp[length - 1] = item;
            _roomArray = temp;
        }

        public void Clear()
        {
            _roomArray = new Room[0];
        }

        public bool Contains(Room item)
        {
            foreach (var some in _roomArray)
                if (some.Equals(item))
                    return true;
            return false;
        }

        public void CopyTo(Room[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<Room> GetEnumerator()
        {
            return new RoomEnum(_roomArray);
        }

        public int IndexOf(Room item)
        {
            for (int i = 0; i < _roomArray.Length; i++)
            {
                if (_roomArray[i].Equals(item))
                    return i;
            }
            return -1;
        }

        public void Insert(int index, Room item)
        {
            _roomArray[index] = item;
        }

        public bool Remove(Room item)
        {
            var retnum = new bool();
            Room[] temp = new Room[_roomArray.Length - 1];
            for (int i = 0, j = 0; i < _roomArray.Length; i++)
            {
                if (!_roomArray[i].Equals(item))
                {
                    temp[j] = _roomArray[i];
                    j++;
                }
                else retnum = true;
            }
            _roomArray = temp;
            return retnum;
        }

        public void RemoveAt(int index)
        {
            Room[] temp = new Room[_roomArray.Length - 1];
            for (int i = 0, j = 0; i < _roomArray.Length; i++)
            {
                if (i != index)
                {
                    temp[j] = _roomArray[i];
                    j++;
                }
            }
            _roomArray = temp;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Sort()
        {
            for (int i = 0; i < _roomArray.Length; i++)
            {
                for (int j = 0; j < _roomArray.Length; j++)
                {
                    if (_roomArray[i].CompareTo(_roomArray[j]) == 1)
                    {
                        Room temp = _roomArray[j];
                        _roomArray[j] = _roomArray[i];
                        _roomArray[i] = temp;
                    }
                }
            }
        }
    }
}
