﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace BLogic.Classes
{

    public class ClientEnum:IEnumerator<Client>
    {
        private Client[] _clientArray;
        private static int _position = -1;

        public ClientEnum(Client[] clientArray)
        {
            _clientArray = clientArray;
        }

        public static int Position
        {
            get
            {
                return _position;
            }

            set
            {
                _position = value;
            }
        }

        public Client Current
        {
            get
            {
                return _clientArray[_position];
            }
        }

        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public bool MoveNext()
        {
            _position += 1;
            return (_position < _clientArray.Length);
        }

        public void Reset()
        {
            _position = -1;
        }
    }
}