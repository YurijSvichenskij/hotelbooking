﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading.Tasks;

using System.Collections;
using BLogic.Interfaces;

namespace BLogic.Classes
{
    public class Client : IComparable<Client>, IEquatable<Client>
    {
        int _idClient;
        string _fio;
        int _phoneNumber;
        int _id;
        DateTime _dateIn;
        DateTime _dateOut;

        public Client()
        {

        }

        public Client(int idClient, string fio, int phonenumber, int id, RoomCollection collection, DateTime dateIn, DateTime dateOut)
        {
            Room temp = new Room();
            temp.Id = id;
            if (!collection.Contains(temp))
            {
                throw new Exception("There is no such room.");
            }

            for (int i=0; i<collection.Count; i++)
            {
                if (collection[i].Equals(temp) && collection[i].Busy == true)
                    throw new Exception("This room is already busy.");
                if (collection[i].Equals(temp))
                {
                    collection[i].Busy = true;
                }
            }

            IdClient = idClient;
            Fio = fio;
            PhoneNumber = phonenumber;
            Id = id;
            DateIn = dateIn;
            DateOut = dateOut;
        }

        public int IdClient
        {
            get
            {
                return _idClient;
            }

            set
            {
                _idClient = value;
            }
        }

        public string Fio
        {
            get
            {
                return _fio;
            }

            set
            {
                _fio = value;
            }
        }

        public int PhoneNumber
        {
            get
            {
                return _phoneNumber;
            }

            set
            {
                _phoneNumber = value;
            }
        }

        public int Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
            }
        }

        public DateTime DateIn
        {
            get
            {
                return _dateIn;
            }

            set
            {
                _dateIn = value;
            }
        }

        public DateTime DateOut
        {
            get
            {
                return _dateOut;
            }

            set
            {
                _dateOut = value;
            }
        }

        public int CompareTo(Client other)
        {
            if (this.IdClient > other.IdClient) return 1;
            else if (this.IdClient < other.IdClient) return -1;
            else return 0;
        }

        public bool Equals(Client other)
        {
            return this.IdClient == other.IdClient;
        }

    }

}
