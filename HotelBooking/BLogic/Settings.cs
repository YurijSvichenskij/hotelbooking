﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLogic
{
    class Settings
    {
        public Settings()
        {
            PathClient = "CollectionClient.XML";
            PathRoom = "CollectionRoom.XML";
            AdminPassword = "admin";
        }

        public string PathClient { get; set; }
        public string PathRoom { get; set; }
        public string AdminPassword { get; set; }

    }
}
