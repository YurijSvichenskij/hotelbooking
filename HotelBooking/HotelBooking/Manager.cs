﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLogic.Classes;
using BLogic.Factories;
using HotelBooking.Views;
using System.IO;
using HotelBooking.Events;

namespace HotelBooking
{
    public class Manager
    {
        public event EventHandler RaiseEvent;
        SomeEventArgs _eventArgs;
        ClientCollection _clientCollection;
        RoomCollection _roomCollection;
        ClientViewModel _cmv;
        RoomViewModel _rmv;
        FileFactory _ff;

        public void DoCheck()
        {
            OnRaiseSomeEvent(this, _eventArgs);
        }

        protected virtual void OnRaiseSomeEvent(object sender, EventArgs e)
        {
            EventHandler handler = RaiseEvent;

            if (handler != null)
            {
                handler(sender, e);
            }
        }

        public void Enter()
        {
            Console.WriteLine("Enter:");
            Console.WriteLine("1)As admin;");
            Console.WriteLine("2)User.");
            var key = Console.ReadKey().Key;
            if (key == ConsoleKey.D1 || key == ConsoleKey.NumPad1)
            {
                _eventArgs.IsAdmin = true;
                Console.Clear();
                Menu();
            }
            else if (key == ConsoleKey.D2 || key == ConsoleKey.NumPad2)
            {
                Console.Clear();
                Menu();
            }
            else Enter();
        }

        public Manager()
        {
            _eventArgs = new SomeEventArgs();
            _clientCollection = new ClientCollection();
            _roomCollection = new RoomCollection();
            _cmv = new ClientViewModel();
            _rmv = new RoomViewModel();
            _ff = new FileFactory();
            RaiseEvent += Manager_RaiseEvent;

            if (File.Exists(_ff.ClientFile.Path))
            {
                _clientCollection = (ClientCollection)_ff.Open(_clientCollection);
            }

            if (File.Exists(_ff.RoomFile.Path))
            {
                _roomCollection = (RoomCollection)_ff.Open(_roomCollection);
            }

            Enter();
        }

        private void Manager_RaiseEvent(object sender, EventArgs e)
        {
            var temp = e as SomeEventArgs;
            if (temp.IsAdmin)
            {

            }
            else
            {
                Console.Clear();
                Console.WriteLine("Acces denied.");
                Console.ReadKey();
                Console.Clear();
                Menu();
            }
        }

        public void Menu()
        {
            try
            {
                Console.Clear();
                Console.WriteLine("Make your choise:");
                Console.WriteLine("1)Admin functions;");
                Console.WriteLine("2)Register room.");
                Console.WriteLine("Press ESCAPE to exit.");
                var key = Console.ReadKey().Key;

                if (key == ConsoleKey.NumPad1 || key == ConsoleKey.D1)
                {
                    DoCheck();
                    AdminManager();
                }
                else if (key == ConsoleKey.NumPad2 || key == ConsoleKey.D2)
                {
                    ClientManager();
                }
                else if (key == ConsoleKey.Escape)
                {
                    _ff.Save(_roomCollection);
                    _ff.Save(_clientCollection);
                    Console.Clear();
                    Environment.Exit(0);
                }
                else throw new Exception("There is no such action");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
                Menu();
            }
        }

        public void ClientManager()
        {
            try
            {
                Console.Clear();
                Console.WriteLine("What do you want to do:");
                Console.WriteLine("1)Show rooms;");
                Console.WriteLine("2)Register room.");
                Console.WriteLine("Press ESCAPE to return to menu.");
                var key = Console.ReadKey().Key;

                if (key == ConsoleKey.NumPad1 || key == ConsoleKey.D1)
                {
                    Console.Clear();
                    _rmv.ShowRoom(_roomCollection);
                    Console.ReadKey();
                    ClientManager();
                }
                else if (key == ConsoleKey.NumPad2 || key == ConsoleKey.D2)
                {
                    Console.Clear();
                    _rmv.ShowRoom(_roomCollection);
                    int idClient;
                    string fio;
                    int phoneNumber;
                    int idRoom;
                    var dateIn = new DateTime();
                    var dateOut = new DateTime();

                    Console.Write("Enter id: ");
                    idClient = Convert.ToInt32(Console.ReadLine());
                    Console.Write("Enter name: ");
                    fio = Console.ReadLine();
                    Console.Write("Enter phone number: ");
                    phoneNumber = Convert.ToInt32(Console.ReadLine());
                    Console.Write("Select room: ");
                    idRoom = Convert.ToInt32(Console.ReadLine());
                    Console.Write("Enter time of beginning: ");
                    dateIn = Convert.ToDateTime(Console.ReadLine());
                    Console.Write("Enter time of out: ");
                    dateOut = Convert.ToDateTime(Console.ReadLine());

                    _clientCollection.Add(new Client(idClient, fio, phoneNumber, idRoom, _roomCollection, dateIn, dateOut));
                    _ff.Save(_clientCollection);
                    _ff.Save(_roomCollection);
                    Console.WriteLine("You was added!");
                    Console.ReadLine();
                    ClientManager();
                }
                else if (key == ConsoleKey.Escape)
                {
                    Menu();
                }
                else throw new Exception("There is no such action");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
                ClientManager();
            }

        }

        public void AdminManager()
        {
            try
            {
                Console.Clear();
                Console.WriteLine("What do you want to do:");
                Console.WriteLine("1)Work with Client;");
                Console.WriteLine("2)Work with Room.");
                Console.WriteLine("Press ESCAPE to return to menu.");
                var key = Console.ReadKey().Key;

                if (key == ConsoleKey.D1 || key == ConsoleKey.NumPad1)
                {
                    ClientManagering();
                }
                else if (key == ConsoleKey.D2 || key == ConsoleKey.NumPad2)
                {
                    RoomManagering();
                }
                else if (key == ConsoleKey.Escape)
                {
                    Menu();
                }
                else throw new Exception("There is no such action.");
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
                AdminManager();
            }
        }

        public void RoomManagering()
        {
            try
            {
                Console.Clear();
                Console.WriteLine("Select an action:");
                Console.WriteLine("1)Add;");
                Console.WriteLine("2)Remove;");
                Console.WriteLine("3)Sort;");
                Console.WriteLine("4)Save;");
                Console.WriteLine("5)Show rooms.");
                Console.WriteLine("Press ESCAPE to return to admin menu.");
                var key = Console.ReadKey().Key;

                if (key == ConsoleKey.D1 || key == ConsoleKey.NumPad1)
                {
                    Console.Clear();
                    _rmv.ShowRoom(_roomCollection);
                    Console.Write("Enter id_room: ");
                    int idRoom = Convert.ToInt16(Console.ReadLine());
                    Console.Write("Enter room cost: ");
                    int cost = Convert.ToInt16(Console.ReadLine());
                    Console.Write("Enter room type: ");
                    string type = Console.ReadLine();
                    _roomCollection.Add(new Room(idRoom, cost, type));
                    _ff.Save(_roomCollection);
                    RoomManagering();
                }
                else if (key == ConsoleKey.D2 || key == ConsoleKey.NumPad2)
                {
                    Console.Clear();
                    if (_roomCollection.Count == 0) throw new Exception("There is no room to remove.");
                    _rmv.ShowRoom(_roomCollection);
                    var temp = new int();

                    Console.Write("Enter id_room: ");
                    temp = Convert.ToInt32(Console.ReadLine());

                    Room tempRoom = new Room();
                    tempRoom.Id = temp;

                    _roomCollection.Remove(tempRoom);
                    _ff.Save(_roomCollection);
                    RoomManagering();
                }
                else if (key == ConsoleKey.D3 || key == ConsoleKey.NumPad3)
                {
                    Console.Clear();
                    _roomCollection.Sort();
                    _ff.Save(_roomCollection);
                    RoomManagering();
                }
                else if (key == ConsoleKey.D4 || key == ConsoleKey.NumPad4)
                {
                    Console.Clear();
                    _rmv.ShowRoom(_roomCollection);
                    _ff.Save(_roomCollection);
                    RoomManagering();
                }
                else if (key == ConsoleKey.D5 || key == ConsoleKey.NumPad5)
                {
                    Console.Clear();
                    _rmv.ShowRoom(_roomCollection);
                    Console.ReadKey();
                    RoomManagering();
                }
                else if (key == ConsoleKey.Escape) AdminManager();
                else throw new Exception("There is no such action.");
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
                RoomManagering();
            }
        }
        public void ClientManagering()
        {
            try
            {
                Console.Clear();
                Console.WriteLine("Select an action:");
                Console.WriteLine("1)Remove;");
                Console.WriteLine("2)Sort;");
                Console.WriteLine("3)Save;");
                Console.WriteLine("4)Show clients.");
                Console.WriteLine("Press ESCAPE to return to admin menu.");
                var key = Console.ReadKey().Key;

                if (key == ConsoleKey.D1 || key == ConsoleKey.NumPad1)
                {
                    Console.Clear();
                    if (_clientCollection.Count == 0) throw new Exception("There is no client to remove.");
                    _cmv.ShowClient(_clientCollection);
                    var temp = new int();

                    Console.Write("Enter id_client: ");
                    temp = Convert.ToInt32(Console.ReadLine());
                    Client tempClient = new Client();
                    tempClient.IdClient = temp;

                    _clientCollection.Remove(tempClient, ref _roomCollection);
                    _ff.Save(_clientCollection);
                    ClientManagering();
                }

                else if (key == ConsoleKey.D3 || key == ConsoleKey.NumPad3)
                {
                    _clientCollection.Sort();
                    _ff.Save(_clientCollection);
                    ClientManagering();
                }
                else if (key == ConsoleKey.D3 || key == ConsoleKey.NumPad3)
                {
                    _ff.Save(_clientCollection);
                    ClientManagering();
                }
                else if (key == ConsoleKey.D4 || key == ConsoleKey.NumPad4)
                {
                    Console.Clear();
                    _cmv.ShowClient(_clientCollection);
                    Console.ReadKey();
                    ClientManagering();
                }
                else if (key == ConsoleKey.Escape)
                {
                    AdminManager();
                }
                else throw new Exception("There is no such action.");
            }
            catch (Exception ex)
            {
                Console.Clear();
                Console.WriteLine(ex.Message);
                Console.ReadKey();
                ClientManagering();
            }

        }
    }
}
