﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelBooking.Events
{
    public class SomeEventArgs : EventArgs
    {
        public SomeEventArgs()
        {
            IsAdmin = false;
        }

        public bool IsAdmin { get; set; }
    }
}
