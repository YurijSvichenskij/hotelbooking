﻿using BLogic.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelBooking.Views
{
    public class RoomViewModel
    {
        public void ShowRoom(RoomCollection collection)
        {
            Console.WriteLine("|-------|--------------------|----------|-------|");
            Console.WriteLine("|{0,-7}|{1,-20}|{2,-10}|{3,-7}|", "ID_ROOM", "COST", "TYPE", "BUSY");
            Console.WriteLine("|-------|--------------------|----------|-------|");
            for(int i = 0; i < collection.Count; i++)
            {
                Console.WriteLine("|{0,-7}|{1,-20}|{2,-10}|{3,-7}|", collection[i].Id, collection[i].Cost, collection[i].Type, collection[i].Busy);
                Console.WriteLine("|-------|--------------------|----------|-------|");
            }
        }
    }
}
