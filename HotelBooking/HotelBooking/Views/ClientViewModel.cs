﻿using BLogic.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelBooking.Views
{
    public class ClientViewModel
    {
        public void ShowClient(ClientCollection collection)
        {
            Console.WriteLine("|-------|--------------------|----------|-------|");
            Console.WriteLine("|{0,-7}|{1,-20}|{2,-10}|{3,-7}|", "ID", "FIO", "TELEPHONE", "IDROOM");
            Console.WriteLine("|-------|--------------------|----------|-------|");
            for (int i = 0; i < collection.Count; i++)
            {
                Console.WriteLine("|{0,-7}|{1,-20}|{2,-10}|{3,-7}|", collection[i].IdClient, collection[i].Fio, collection[i].PhoneNumber, collection[i].Id);
                Console.WriteLine("|-------|--------------------|----------|-------|");
            }
        }
    }
}
