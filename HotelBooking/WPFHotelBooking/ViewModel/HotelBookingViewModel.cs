﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPFHotelBooking.Model;

namespace WPFHotelBooking.ViewModel
{
    public class HotelBookingViewModel:HotelBookingViewModelBase
    {
        public HotelBookingModel model { get; set; }
        private DataTable _clients;
        private DataTable _rooms;
        public DataTable Clients
        {
            get
            {
                return _clients;
            }
            set
            {
                _clients = value;
                OnPropertyChanged("Clients");
                model.Data.LoadDataIntoDatabase();
            }
        }
        public DataTable Rooms
        {
            get
            {
                return _rooms;
            }
            set
            {
                _rooms = value;
                OnPropertyChanged("Rooms");
                model.Data.LoadDataIntoDatabase();
            }
        }

        public HotelBookingViewModel()
        {
            model = new HotelBookingModel();
            model.Data.LoadDataFromDatabase();
            Clients = model.Data.Clients;
            Rooms = model.Data.Rooms;
        }
    }
}
