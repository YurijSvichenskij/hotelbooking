﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WPFHotelBooking
{
    /// <summary>
    /// Логика взаимодействия для Admin.xaml
    /// </summary>
    public partial class Admin : Window
    {
        public Admin()
        {
            InitializeComponent();
        }

        private void Addroom_Checked(object sender, RoutedEventArgs e)
        {
            labelCost.Visibility = Visibility.Visible;
            textCost.Visibility = Visibility.Visible;
            labelType.Visibility = Visibility.Visible;
            textTypeRoomClient.Visibility = Visibility.Visible;

            labelRoom.Visibility = Visibility.Hidden;
            labelClient.Visibility = Visibility.Hidden;
        }
        private void Removeclient_Checked(object sender, RoutedEventArgs e)
        {
            textTypeRoomClient.Visibility = Visibility.Visible;
            labelRoom.Visibility = Visibility.Visible;

            labelCost.Visibility = Visibility.Hidden;
            textCost.Visibility = Visibility.Hidden;
            labelType.Visibility = Visibility.Hidden;
            labelClient.Visibility = Visibility.Hidden;
        }
        private void Removeroom_Checked(object sender, RoutedEventArgs e)
        {
            labelClient.Visibility = Visibility.Visible;
            textTypeRoomClient.Visibility = Visibility.Visible;

            labelCost.Visibility = Visibility.Hidden;
            textCost.Visibility = Visibility.Hidden;
            labelType.Visibility = Visibility.Hidden;
            labelRoom.Visibility = Visibility.Hidden;
        }

        private void AppClosed(object sender, EventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
