﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFHotelBooking
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonAdmin_Click(object sender, RoutedEventArgs e)
        {
            Admin AdminWindow = new Admin();
            this.Hide();
            AdminWindow.Show();
        }

        private void ButtonClient_Click(object sender, RoutedEventArgs e)
        {
            Client AdminClietn = new Client();
            this.Hide();
            AdminClietn.Show();
        }
    }
}
