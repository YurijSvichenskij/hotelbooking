﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer;

namespace WPFHotelBooking.Model
{
    public class HotelBookingModel
    {
        public DataAccess Data { get; set; }

        public HotelBookingModel()
        {
            Data = new DataAccess();
        }
    }
}
