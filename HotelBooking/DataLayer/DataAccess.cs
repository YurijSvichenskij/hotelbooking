﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class DataAccess
    {
        SqlConnectionStringBuilder connectionStringBuilder;
        SqlDataAdapter dataAdapterClient;
        SqlDataAdapter dataAdapterRoom;
        SqlDataAdapter dataAdapterContract;
        SqlConnection connection;
        SqlCommandBuilder commandBuilderClient;
        SqlCommandBuilder commandBuilderRoom;
        SqlCommandBuilder commandBuilderContract;
        DataSet dataSet;
        public DataTable Clients { get; set; }
        public DataTable Rooms { get; set; }
        public DataTable Contracts { get; set; }

        public DataAccess()
        {
            connectionStringBuilder = new SqlConnectionStringBuilder();
            connectionStringBuilder.IntegratedSecurity = true;
            connectionStringBuilder.InitialCatalog = "HotelBooking"; //name of database
            connectionStringBuilder.DataSource = "DESKTOP-TBJ6DRK"; //name of server

            connection = new SqlConnection(connectionStringBuilder.ConnectionString);

            dataAdapterClient = new SqlDataAdapter("select * from Client", connection);
            dataAdapterRoom = new SqlDataAdapter("select * from Room", connection);
            dataAdapterContract = new SqlDataAdapter("select * from Contract", connection);
            dataAdapterClient.MissingSchemaAction = MissingSchemaAction.AddWithKey;
            dataAdapterRoom.MissingSchemaAction = MissingSchemaAction.AddWithKey;
            dataAdapterContract.MissingSchemaAction = MissingSchemaAction.AddWithKey;

            commandBuilderClient = new SqlCommandBuilder(dataAdapterClient);
            commandBuilderRoom = new SqlCommandBuilder(dataAdapterRoom);
            commandBuilderContract = new SqlCommandBuilder(dataAdapterRoom);

            dataSet = new DataSet("HotelBookingDataSet");
        }

        public void LoadDataFromDatabase()
        {
            try
            {
                connection.Open();

                dataAdapterClient.Fill(dataSet, "Client");
                dataAdapterRoom.Fill(dataSet, "Room");
                dataAdapterContract.Fill(dataSet, "Contract");

                Clients = dataSet.Tables["Client"];
                Rooms = dataSet.Tables["Room"];
                Contracts = dataSet.Tables["Contract"];
            }
            finally
            {
                connection.Close();
            }
        }

        public void LoadDataIntoDatabase()
        {
            dataAdapterClient.Update(dataSet, "Client");
            dataAdapterRoom.Update(dataSet, "Room");
            dataAdapterContract.Update(dataSet, "Contract");
        }

        public void AddClient(string fio, string phonenumber, string password, DateTime dateIn, DateTime dateOut, string idRoom)
        {
            DataRow currentClient = Clients.NewRow();
            currentClient["id"] = Guid.NewGuid().ToString();
            currentClient["fio"] = fio;
            currentClient["phonenumber"] = phonenumber;
            currentClient["datein"] = dateIn;
            currentClient["dateout"] = dateOut;

            DataRow currentContract = Contracts.NewRow();
            currentContract["id"] = Guid.NewGuid().ToString();
            currentContract["idclient"] = currentClient["id"];
            currentContract["idroom"] = idRoom;

            Clients.Rows.Add(currentClient);
            Contracts.Rows.Add(currentContract);
            UpdateRoom(idRoom, "yes");
        }

        public void DeleteClient(string id)
        {
            DataRow currentClient = Clients.Rows.Find(id);
            DataRow currentContract = null;

            foreach (DataRow item in Contracts.Rows)
            {
                if ((string)item["idclient"] == id)
                {
                    currentContract = item;
                    break;
                }
            }

            UpdateRoom((string)currentContract["idroom"], "not");
            currentContract.Delete();
            currentClient.Delete();
        }

        public void AddRoom(string cost, string type)
        {
            DataRow currentRoom = Rooms.NewRow();
            currentRoom["id"] = Guid.NewGuid().ToString();
            currentRoom["cost"] = cost;
            currentRoom["type"] = type;
            currentRoom["busy"] = "not";

            Rooms.Rows.Add(currentRoom);
        }

        public void UpdateRoom(string id, string busy)
        {
            DataRow current = Rooms.Rows.Find(id);

            current.BeginEdit();
            current["busy"] = busy;
            current.EndEdit();
        }

        public void DeleteRoom(string id)
        {
            DataRow current = Rooms.Rows.Find(id);
            current.Delete();
        }

        /*public void UpdateUser(int index, string email, string password)
        {
            try
            {
                DataRow current = Clients.Rows[index];

                current.BeginEdit();
                current["email"] = email;
                current["password"] = password;
                current.EndEdit();
            }
            catch (Exception ex)
            {
                Console.Clear();
                Console.WriteLine(ex.Message);
                Console.ReadKey();
                Console.Clear();
            }
        }

        public void UpdateUser(string id, string email, string password)
        {
            try
            {
                DataRow current = Clients.Rows.Find(id);

                current.BeginEdit();
                current["email"] = email;
                current["password"] = password;
                current.EndEdit();
            }
            catch (Exception ex)
            {
                Console.Clear();
                Console.WriteLine(ex.Message);
                Console.ReadKey();
                Console.Clear();
            }
        }

        public void DeleteUser(int index)
        {
            try
            {
                DataRow current = Clients.Rows[index];
                current.Delete();
            }
            catch (Exception ex)
            {
                Console.Clear();
                Console.WriteLine(ex.Message);
                Console.ReadKey();
                Console.Clear();
            }
        }

        public void DeleteUser(string id)
        {
            try
            {
                DataRow current = Clients.Rows.Find(id);
                current.Delete();
            }
            catch (Exception ex)
            {
                Console.Clear();
                Console.WriteLine(ex.Message);
                Console.ReadKey();
                Console.Clear();
            }
        }*/

        public DataTable GetClients()
        {
            return Clients;
        }
    }
}
